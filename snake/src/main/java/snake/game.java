package snake;

import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.DefaultWindowManager;
import com.googlecode.lanterna.gui2.EmptySpace;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.TextBox;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

import java.io.IOException;

enum Direction {
	UP, RIGHT, DOWN, LEFT;
	
	public static boolean isOpposite(Direction direction, Direction direction2) {
		if (direction == Direction.LEFT && direction2 == Direction.RIGHT) {
			return true; 
		} else if (direction == Direction.RIGHT && direction2 == Direction.LEFT) {
			return true; 
		} else if (direction == Direction.UP && direction2 == Direction.DOWN) {
			return true; 
		} else if (direction == Direction.DOWN && direction2 == Direction.UP) {
			return true; 
		} else {
			return false;
		}
	}
}

class Snake {
	
	ArrayList<Point> body = new ArrayList<Point>();
	{
		body.add(new Point(10,10,"@"));
		body.add(new Point(10,9,"@"));
		body.add(new Point(10,8,"@"));
	}
	
	Direction direction = Direction.RIGHT;
	
	Point food = new Point((int)(Math.random() * game.TERMINAL_ROWS-1), (int)(Math.random() * game.TERMINAL_COLUMNS-1), "*");

	public void move() {
		
		switch (direction) {
			case UP:
				body.add(0, new Point(body.get(0).row - 1, body.get(0).column, "@")); 
				break;
			case DOWN:
				body.add(0, new Point(body.get(0).row + 1, body.get(0).column,  "@")); 
				break;
			case LEFT:
				body.add(0, new Point(body.get(0).row, body.get(0).column - 1, "@")); 
				break;
			case RIGHT:
				body.add(0, new Point(body.get(0).row, body.get(0).column + 1, "@")); 
				break;
			default:
				break;
				
		}
		
		if (!food.isMatch(body.get(0))) {
			body.get(body.size()-1).delete();
			body.remove(body.size()-1);
		} else {
			this.ChangeFood();
		}
		
		for (int i = 1; i < body.size(); i++) {
			if (body.get(i).column == body.get(0).column && body.get(i).row == body.get(0).row) {
				Cycle.GAME = false;
			}	
		}
		
		if (body.get(0).column > game.TERMINAL_COLUMNS - 1 
				|| body.get(0).column < 0 
				|| body.get(0).row > game.TERMINAL_ROWS - 1 
				|| body.get(0).row < 0) {
			Cycle.GAME = false;
		}
		
	}
	
	public void ChangeDirection(Direction direction) {
		if (!Direction.isOpposite(direction, this.direction)) {
			this.direction = direction;
		}
	}
	
	public void ChangeFood() {
		int fc, fr;
		boolean flag = true;
		do {
			fc = (int)(Math.random() * game.TERMINAL_COLUMNS - 1);
			fr = (int)(Math.random() * game.TERMINAL_ROWS - 1);
			
			for (int i = 0; i < this.body.size(); i++) {
				if (body.get(i).column == fc && body.get(i).row == fr) {
					flag = true;
					break; 
				} else {
					flag = false;
				}
			}
			
		} while (flag);
		
		this.food.column = fc;
		this.food.row = fr;
	}
	
	public boolean isPointOnSnake(Point point) {
		for (int i = 0; i < this.body.size(); i++) {
			if (body.get(i).column == point.column && body.get(i).row == point.row) {
				return true;
			} 
		}
		return false;
	}
}

class Point {
	int row;
	int column;
	String view;
	
	public static HashSet<Point> instances = new HashSet<Point>();
	
	public Point(int row, int column, String view) {
	      instances.add(this);
	      this.column = column;
	      this.row = row;
	      this.view = view;
	}
	
	public void delete() {
		instances.remove(this);
	}
	
	public boolean isMatch(Point point) {
		if (this.column == point.column && this.row == point.row) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isMatch(int row, int column) {
		if (this.column == column && this.column == row) {
			return true;
		} 
		return false;
	}
	
	public void setCoordinates(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public void setRow(int row) {
		this.row = row;
	}
	
	public void setColumn(int column) {
		this.column = column;
	}
}

class Cycle extends Thread {
	
	public static boolean GAME = true;
	
    public void run(Screen screen, Snake snake) throws IOException{
    	
	    try {
	    	while (GAME == true) {
	    		Thread.sleep(80);
	    		
	    		screen.clear();
	    		    
	    		screen.newTextGraphics();
	    		for (Point pnt : Point.instances) {
	    			screen.newTextGraphics().setModifiers(EnumSet.of(SGR.BOLD)).putString(pnt.column, pnt.row, pnt.view);
				}
	    		
	    		screen.refresh();
	    		
	    		snake.move();
	 
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	           
    }
}


class KeyPressedListener implements NativeKeyListener {
	Snake snake;
	Screen screen;
	Terminal terminal;
	
	public KeyPressedListener(Snake snake, Screen screen, Terminal terminal) {
		this.screen = screen;
		this.snake = snake;
		this.terminal = terminal;
	}
	
	public void nativeKeyPressed(NativeKeyEvent e) {
        
		try {
			TimeUnit.MILLISECONDS.sleep(80);
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
        switch (e.getKeyCode()) {
		case 57416:
			snake.ChangeDirection(Direction.UP);
			break;
		case 57421:
			snake.ChangeDirection(Direction.RIGHT);
			break;
		case 57424:
			snake.ChangeDirection(Direction.DOWN);
			break;
		case 57419:
			snake.ChangeDirection(Direction.LEFT);
			break;
		default:
			break;
		} 

		if (e.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
            		try {
                		GlobalScreen.unregisterNativeHook();
                		try {
							terminal.close();
							System.exit(1);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
            		} catch (NativeHookException nativeHookException) {
                		nativeHookException.printStackTrace();
            		}
        	}
	}
	
	public void nativeKeyReleased(NativeKeyEvent e) {
		// TODO Auto-generated method stub
	}

	public void nativeKeyTyped(NativeKeyEvent e) {
		// TODO Auto-generated method stub
	}
}

public class game {
	
	public static SwingTerminalFrame terminal;
    public static Screen screen;
    public static final int TERMINAL_ROWS = 20;
    public static final int TERMINAL_COLUMNS = 30;
    
	public static void main(String[] args) throws IOException {
		
		Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        logger.setUseParentHandlers(false);
		
        try {
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException e) {
			//TODO Auto-generated catch block
			System.err.println("There was a problem registering the native hook.");

            System.exit(1);
		}
        
        // Setup terminal and screen layers
        terminal = new DefaultTerminalFactory().setTerminalEmulatorTitle("Snake")
        		.setInitialTerminalSize(new TerminalSize(TERMINAL_COLUMNS, TERMINAL_ROWS))
        		.createSwingTerminal();
       
        terminal.setVisible(true);
        terminal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);        
        terminal.setCursorVisible(false);
        
        screen = new TerminalScreen(terminal);  
        screen.startScreen();

        final BasicWindow window = new BasicWindow();
        
        Panel panel = new Panel();
        panel.setLayoutManager(new LinearLayout(com.googlecode.lanterna.gui2.Direction.VERTICAL));

        panel.addComponent(new Label("Версия 1.1.1"));
        panel.addComponent(new Button("Старт", new Runnable() {
	    		public void run() {
	    			window.close();
	    		}
	    	}
        ));
        panel.addComponent(new Button("Выход", new Runnable() {
	    		public void run() {
	    			terminal.close();
	    			System.exit(0);
				}
			}
        ));
        
        window.setComponent(panel.withBorder(Borders.singleLine("Змейка от Макса")));

        // Create gui and start gui
        MultiWindowTextGUI gui = new MultiWindowTextGUI(screen, new DefaultWindowManager(), new EmptySpace(TextColor.ANSI.BLUE));
        gui.addWindow(window);
        gui.waitForWindowToClose(window);
        
        
        
        Snake snake = new Snake();
        
        GlobalScreen.addNativeKeyListener(new KeyPressedListener(snake, screen, terminal));

        new Cycle().run(screen, snake);
               
	}

}
